//
//  Utils.h
//  Ecomova
//
//  Created by Ewerton Pereira on 14/09/13.
//  Copyright (c) 2013 Ewerton Pereira. All rights reserved.
//

#ifndef Ecomova_Utils_h
#define Ecomova_Utils_h

#define kPAGE_MY_LOCALS   @"mylocals"
#define kPAGE_ITINERARIES @"itineraries"
#define kPAGE_PEOPLE      @"people"
#define kPAGE_CONTACTS    @"contacts"
#define kPAGE_AGENDA      @"agenda"

#define kMENU_ITEM_CHOOSED @"itemchoosed"

#define HEXCOLOR(c) [UIColor colorWithRed:((c>>24)&0xFF)/255.0 green:((c>>16)&0xFF)/255.0 blue:((c>>8)&0xFF)/255.0 alpha:((c)&0xFF)/255.0];


#endif
