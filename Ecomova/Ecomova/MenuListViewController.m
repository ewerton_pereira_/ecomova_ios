//
//  ListViewController.m
//  Prototype
//
//  Created by Ewerton Pereira on 08/09/13.
//  Copyright (c) 2013 Ewerton Pereira. All rights reserved.
//

#import "MenuListViewController.h"
#import "MenuItemView.h"
#import "Utils.h"

@interface MenuListViewController (){
    NSArray *menuItens;
    
    IBOutlet MenuItemView *myLocals, *itineraries, *people;
    int currentPage;
}

@end

@implementation MenuListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    CGRect frame = self.view.frame;
    frame.origin.y = 0;
    frame.size.height = 568;
    self.view.frame  = frame;

    MenuItemView * menuItem;
    menuItens = [NSArray arrayWithObjects:myLocals,itineraries,people, nil];
    for (int i= 0; i < [menuItens count]; i++){
        menuItem = (MenuItemView*)[menuItens objectAtIndex:i];
        menuItem.name.text = NSLocalizedString(menuItem.name.text, nil);
    }
    
    currentPage =  ((MenuItemView*)[menuItens objectAtIndex:0]).tag;
}

-(IBAction)menuItemTouched:(UIButton *)sender{
    
    if (sender.tag != currentPage){
        currentPage = sender.tag;
    switch (sender.tag) {
        case 1:
            [[NSNotificationCenter defaultCenter]postNotificationName:kPAGE_MY_LOCALS object:nil];
            break;
        case 2:
            [[NSNotificationCenter defaultCenter]postNotificationName:kPAGE_ITINERARIES object:nil];
            break;
        case 3:
            [[NSNotificationCenter defaultCenter]postNotificationName:kPAGE_PEOPLE object:nil];
            break;
        case 5:
            [[NSNotificationCenter defaultCenter]postNotificationName:kPAGE_PEOPLE object:nil];
            break;
       
    }
    }
    [[NSNotificationCenter defaultCenter]postNotificationName:kMENU_ITEM_CHOOSED object:nil];
    
}

-(IBAction)dismissMenuViewTouched:(id)sender{
   [[NSNotificationCenter defaultCenter]postNotificationName:kMENU_ITEM_CHOOSED object:nil]; 
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
