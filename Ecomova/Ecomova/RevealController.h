//
//  RevealController.h
//  Prototype
//
//  Created by Ewerton Pereira on 08/09/13.
//  Copyright (c) 2013 Ewerton Pereira. All rights reserved.
//

#import "ZUUIRevealController.h"

@interface RevealController : ZUUIRevealController <ZUUIRevealControllerDelegate>

-(void)revealToggle:(id)sender;


+(RevealController *)instance;

@end
