//
//  main.m
//  Ecomova
//
//  Created by Ewerton Pereira on 12/09/13.
//  Copyright (c) 2013 Ewerton Pereira. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
