//
//  AgendaViewController.m
//  Ecomova
//
//  Created by Ewerton Pereira on 07/10/13.
//  Copyright (c) 2013 Ewerton Pereira. All rights reserved.
//

#import "AgendaViewController.h"
#import <EventKit/EventKit.h>

@interface AgendaViewController ()

@end

@implementation AgendaViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    /*
    EKEventStore *eventDB = [[EKEventStore alloc] init];
    NSArray * calendars = [eventDB calendars];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    NSString * typeString = @"";
    
    for (EKCalendar *thisCalendar in calendars) {
        EKCalendarType type = thisCalendar.type;
        if (type == EKCalendarTypeLocal) {
            typeString = @"local";
        }
        if (type == EKCalendarTypeCalDAV) {
            typeString = @"calDAV";
        }
        if (type == EKCalendarTypeExchange) {
            typeString = @"exchange";
        }
        if (type == EKCalendarTypeSubscription) {
            typeString = @"subscription";
        }
        if (type == EKCalendarTypeBirthday) {
            typeString = @"birthday";
        }
        if (thisCalendar.allowsContentModifications) {
            NSLog(@"The title is:%@", thisCalendar.title);
            [dict setObject: typeString forKey: thisCalendar.title];
        }
    }*/
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
