//
//  MenuItemView.h
//  Ecomova
//
//  Created by Ewerton Pereira on 14/09/13.
//  Copyright (c) 2013 Ewerton Pereira. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuItemView : UIView

@property (strong ) IBOutlet UILabel *name;

@end
