//
//  FirstListViewController.h
//  Prototype
//
//  Created by Ewerton Pereira on 08/09/13.
//  Copyright (c) 2013 Ewerton Pereira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddPlaceViewController.h"

@interface MyPlacesViewController : UITableViewController<AddplaceDelegate>
@property(nonatomic, assign) NSUInteger nbItems;
@end
