//
//  RootNavController.m
//  Prototype
//
//  Created by Ewerton Pereira on 08/09/13.
//  Copyright (c) 2013 Ewerton Pereira. All rights reserved.
//

#import "RootNavController.h"
#import "Utils.h"


@interface RootNavController ()

@end

@implementation RootNavController

-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    
    //self.navigationBar.tintColor = [UIColor grayColor];
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
   // self.navigationBar.tintColor = HEXCOLOR(0xffffffff);
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(gotoPageLocals)
                                                 name:kPAGE_MY_LOCALS object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(gotoPageItineraries)
                                                 name:kPAGE_ITINERARIES object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(gotoPagePeople)
                                                 name:kPAGE_PEOPLE object:nil];

}


-(void)gotoPageLocals{
    NSLog(@"pag locals");
    [self popToRootViewControllerAnimated:YES];
}


-(void)gotoPageItineraries{
    UIViewController *vc = [[UIStoryboard storyboardWithName:@"iPhoneStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"itinerariosviewcontroller"];
    [self pushViewController:vc animated:YES];
    
}

-(void)gotoPagePeople{
    UIViewController *vc = [[UIStoryboard storyboardWithName:@"iPhoneStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"peopleviewcontroller"];
    [self pushViewController:vc animated:YES];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
