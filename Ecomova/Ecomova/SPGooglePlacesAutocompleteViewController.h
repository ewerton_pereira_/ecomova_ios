//
//  SPGooglePlacesAutocompleteViewController.h
//  SPGooglePlacesAutocomplete
//
//  Created by Stephen Poletto on 7/17/12.
//  Copyright (c) 2012 Stephen Poletto. All rights reserved.
//

#import <MapKit/MapKit.h>

@protocol AddplaceDelegate <NSObject>
-(void) placeChoosen : (NSDictionary *) place ;
@end


@class SPGooglePlacesAutocompleteQuery;

@interface SPGooglePlacesAutocompleteViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UISearchDisplayDelegate, UISearchBarDelegate, MKMapViewDelegate> {
    NSArray *searchResultPlaces;
    SPGooglePlacesAutocompleteQuery *searchQuery;
    MKPointAnnotation *selectedPlaceAnnotation;
    
    BOOL shouldBeginEditing;
    id <AddplaceDelegate> __unsafe_unretained  delegate;
    NSDictionary *placeDict;
}

@property (unsafe_unretained) id <AddplaceDelegate> delegate;
@property (retain, nonatomic) IBOutlet MKMapView *mapView;

@end
