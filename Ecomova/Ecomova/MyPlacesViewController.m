//
//  FirstListViewController.m
//  Prototype
//
//  Created by Ewerton Pereira on 08/09/13.
//  Copyright (c) 2013 Ewerton Pereira. All rights reserved.
//

#import "MyPlacesViewController.h"
#import "MCSwipeTableViewCell.h"
#import "SPGooglePlacesAutocompleteViewController.h"

@interface MyPlacesViewController ()  <MCSwipeTableViewCellDelegate>{
    NSMutableArray *places;
}


@end

@implementation MyPlacesViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
   
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    places = [[NSMutableArray alloc]init];
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    self.title = NSLocalizedString(@"Meus locais", nil);
    
	if ([self.navigationController.parentViewController respondsToSelector:@selector(revealGesture:)] && [self.navigationController.parentViewController respondsToSelector:@selector(revealToggle:)])
	{
		UIPanGestureRecognizer *navigationBarPanGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self.navigationController.parentViewController action:@selector(revealGesture:)];
		[self.navigationController.navigationBar addGestureRecognizer:navigationBarPanGestureRecognizer];
		
		UIBarButtonItem *item = self.navigationItem.leftBarButtonItem ;
        [item setTarget:self.navigationController.parentViewController];
        [item setAction:@selector(revealToggle:)];
	}
    

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - addplaces delegate
-(void)placeChoosen:(NSDictionary *)place{
    
    [places addObject:place];
    [self reload];
    
    self.navigationController.navigationBarHidden = NO;
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - button actions
-(IBAction)addPlaceTouched:(id)sender{
    
    SPGooglePlacesAutocompleteViewController *addPLaceVC =[[SPGooglePlacesAutocompleteViewController alloc] init];
    addPLaceVC.delegate = self;
    
    [self.navigationController pushViewController:addPLaceVC animated:YES];
}


#pragma mark - prepare for segue delegate
// deprecated
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"gotoaddplace"]) {
       
        AddPlaceViewController *addPlaceVC = (AddPlaceViewController *) segue.destinationViewController;
        addPlaceVC.delegate = self;
    }
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [places count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    MCSwipeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell) {
        cell = [[MCSwipeTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    [cell setDelegate:self];
    [cell setFirstStateIconName:@"check.png"
                     firstColor:[UIColor colorWithRed:85.0 / 255.0 green:213.0 / 255.0 blue:80.0 / 255.0 alpha:1.0]
            secondStateIconName:@"check.png"
                    secondColor:[UIColor colorWithRed:85.0 / 255.0 green:213.0 / 255.0 blue:80.0 / 255.0 alpha:1.0]
                  thirdIconName:@"pedir.png"
                     thirdColor:[UIColor colorWithRed:254.0 / 255.0 green:217.0 / 255.0 blue:56.0 / 255.0 alpha:1.0]
                 fourthIconName:@"pedir.png"
                    fourthColor:[UIColor colorWithRed:254.0 / 255.0 green:217.0 / 255.0 blue:56.0 / 255.0 alpha:1.0]];
    
    [cell.contentView setBackgroundColor:[UIColor whiteColor]];
    [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
    
    
    NSDictionary *place =  [places objectAtIndex:indexPath.row];
    
    [cell.textLabel setText:[place objectForKey:@"title" ]];
    [cell.detailTextLabel setText:[place objectForKey:@"address"]];
    [cell setMode:MCSwipeTableViewCellModeSwitch];
    NSLog(@"cell %f", tableView.superview.frame.origin.x);
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80.0;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

#pragma mark - MCSwipeTableViewCellDelegate

- (void)swipeTableViewCell:(MCSwipeTableViewCell *)cell didTriggerState:(MCSwipeTableViewCellState)state withMode:(MCSwipeTableViewCellMode)mode {
    NSLog(@"IndexPath : %@ - MCSwipeTableViewCellState : %d - MCSwipeTableViewCellMode : %d", [self.tableView indexPathForCell:cell], state, mode);
    
    if (mode == MCSwipeTableViewCellModeExit) {
        //_nbItems--;
       // [self.tableView deleteRowsAtIndexPaths:@[[self.tableView indexPathForCell:cell]] withRowAnimation:UITableViewRowAnimationFade];
    }
}

#pragma mark -

- (void)reload {
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
}

@end
