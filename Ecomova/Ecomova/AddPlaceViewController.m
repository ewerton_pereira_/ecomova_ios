//
//  AddPlaceViewController.m
//  Ecomova
//
//  Created by Ewerton Pereira on 19/09/13.
//  Copyright (c) 2013 Ewerton Pereira. All rights reserved.
//

#import "AddPlaceViewController.h"

#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0) //1


@interface AddPlaceViewController (){
    UISearchBar *searchBar;
    IBOutlet MKMapView *mapView;
    NSDictionary *place;
}

@end

@implementation AddPlaceViewController

@synthesize delegate;

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.hidesBackButton = true;
    searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(-5.0, 0.0, 320.0, 44.0)];
    searchBar.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    UIView *searchBarView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 310.0, 44.0)];
    searchBarView.autoresizingMask = 0;
    searchBar.delegate = self;
    [searchBarView addSubview:searchBar];
    self.navigationItem.titleView = searchBarView;
    
    mapView.showsUserLocation = YES;
    NSLog(@"coord %f",mapView.userLocation.coordinate.latitude);
    CLLocationCoordinate2D startCoord = mapView.userLocation.coordinate;
    MKCoordinateRegion adjustedRegion = [mapView regionThatFits:MKCoordinateRegionMakeWithDistance(startCoord, 800, 800)];
    [mapView setRegion:adjustedRegion animated:YES];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - buttons action
-(IBAction)addPlaceTouched:(id)sender{
    
    if (place)
    [delegate placeChoosen:place];
}

-(IBAction)cancelAdd:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - CLLocation delegate
-(void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation{

    NSLog(@"teste1");
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    
    NSLog(@"teste2");
}

#pragma mark - search place code - search bar delegates

- (void) searchBarSearchButtonClicked:(UISearchBar *)theSearchBar
{
    //Perform the JSON query.
    [self searchCoordinatesForAddress:[searchBar text]];
    
    //Hide the keyboard.
    [searchBar resignFirstResponder];
}

- (void) searchCoordinatesForAddress:(NSString *)inAddress
{
    NSString *geocodingBaseUrl = @"http://maps.googleapis.com/maps/api/geocode/json?";
    NSString *url = [NSString stringWithFormat:@"%@address=%@&sensor=false", geocodingBaseUrl,inAddress];
    url = [url stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding];
    
    NSURL *queryUrl = [NSURL URLWithString:url];
    dispatch_sync(kBgQueue, ^{
        
        NSData *data = [NSData dataWithContentsOfURL: queryUrl];
        
        [self fetchedData:data];
        
    });
    
    
}

- (void)fetchedData:(NSData *)data {
    
    NSError* error;
    NSDictionary *json = [NSJSONSerialization
                          JSONObjectWithData:data
                          options:kNilOptions
                          error:&error];
    
    NSArray* results = [json objectForKey:@"results"];
    NSDictionary *result = [results objectAtIndex:0];
    NSString *address = [result objectForKey:@"formatted_address"];
    NSDictionary *geometry = [result objectForKey:@"geometry"];
    NSDictionary *location = [geometry objectForKey:@"location"];
    NSString *lat = [location objectForKey:@"lat"];
    NSString *lng = [location objectForKey:@"lng"];
    
    
    NSArray *arrAddress = [address componentsSeparatedByString:@","];
    NSString *title =  [arrAddress objectAtIndex:0];
    address = @"";
    if ([arrAddress count] > 1) {
        address = [arrAddress objectAtIndex:1];
        for  (int i=2 ; i < [arrAddress count] ; i++){
            address = [NSString stringWithFormat:@"%@, %@", address ,[arrAddress objectAtIndex:i ]];
        }
    }
    
    place = [[NSDictionary alloc]initWithObjectsAndKeys:lat,@"lat",lng,@"lng",title,@"title", address,@"address",nil];
   
    NSLog(@"CODEC2 %@",results);
   
    [self zoomMapAndCenterAtLatitude:[[place objectForKey:@"lat"]doubleValue] andLongitude:[[place objectForKey:@"lng"]doubleValue]];
    
}


- (void) zoomMapAndCenterAtLatitude:(double) latitude andLongitude:(double) longitude
{
    
    MKCoordinateRegion region;
    region.center.latitude  = latitude;
    region.center.longitude = longitude;
    
    //Set Zoom level using Span
    MKCoordinateSpan span;
    span.latitudeDelta  = .03;
    span.longitudeDelta = .03;
    region.span = span;

    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
    CLLocationCoordinate2D coordinate;
    // Set annotation to point at coordinate
    coordinate.latitude = latitude;
    coordinate.longitude = longitude;
    point.coordinate = coordinate;
    
    for (id annotation in mapView.annotations) {
        [mapView removeAnnotation:annotation];
    }
    
    [mapView addAnnotation:point];
    
    //Move the map and zoom
    [mapView setRegion:region animated:YES];
}

@end
