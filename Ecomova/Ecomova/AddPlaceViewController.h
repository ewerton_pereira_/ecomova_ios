//
//  AddPlaceViewController.h
//  Ecomova
//
//  Created by Ewerton Pereira on 19/09/13.
//  Copyright (c) 2013 Ewerton Pereira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@protocol AddplaceDelegate <NSObject>

-(void) placeChoosen : (NSDictionary *) place ;

@end

@interface AddPlaceViewController : UIViewController <UISearchBarDelegate, CLLocationManagerDelegate>{
    id <AddplaceDelegate> __unsafe_unretained  delegate;
}

@property (unsafe_unretained) id <AddplaceDelegate> delegate;

@end
